import os
import tensorflow as tf
from flask import Flask, request,jsonify
from flask_cors import CORS

from cvparser.parse import Parser
from configuration import tempStorage


app = Flask(__name__)
CORS(app, headers="X-CSRFToken, Content-Type")

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
global graph
parser_obj = Parser()
graph= tf.get_default_graph()

@app.route("/parse", methods=["POST", "GET"])
def getAge():
    resume = request.files.getlist('file')
    total_response = []
    if resume:
        for res in resume:
            filename = res.filename
            res.save(tempStorage+'/'+filename)

            resume_path = tempStorage+'/'+filename
            with graph.as_default():
                parsed_data = parser_obj.resume_parser(resume_path)
                total_response.append(parsed_data)
        return jsonify(total_response)




if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8001, debug=True)
