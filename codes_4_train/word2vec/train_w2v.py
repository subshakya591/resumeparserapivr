from gensim.models import Word2Vec
from sklearn.decomposition import PCA
from matplotlib import pyplot
from resumeparsernew.codes_4_train.word2vec.parse_data import W2vData
from resumeparsernew.codes_4_train.config import ROOT

training_data = W2vData()
sentences = training_data.prepare_training_text()
# print(sentences)

# train model
model = Word2Vec(sentences, size=100, window=5, min_count=2, workers=4)
# summarize the loaded model
# summarize vocabulary
words = list(model.wv.vocab)
# access vector for one word
# print(model['developer'])
# save model
model.save(ROOT+'/../cvscorer/models/model.bin')

X = model[model.wv.vocab]
pca = PCA(n_components=2)
result = pca.fit_transform(X)
# create a scatter plot of the projection
pyplot.scatter(result[:, 0], result[:, 1])
words = list(model.wv.vocab)
for i, word in enumerate(words):
	pyplot.annotate(word, xy=(result[i, 0], result[i, 1]))
pyplot.show()
