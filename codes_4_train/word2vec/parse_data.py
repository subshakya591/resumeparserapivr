import re
import os

import docx2txt
from tika import parser

from resumeparsernew.codes_4_train import config as cfg

import nltk


class W2vData:

    def __init__(self):
        # self.input_data = {}
        self.input_data = cfg.data_path# for path, subdirs, files in os.walk(
        #     self.input_data[path] = {# for path, subdirs, files in os.walk( files}
        # print(self.input_data)
                 



    def clean_text(self,text):
        try:
            text = text.lower()
            text = re.sub(r'((http|ftp|https):\/\/)?[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?', '', text)
            text = re.sub(r'[|:}{=]', ' ',text)
            text = re.sub(r'[;]', ' ',text)
            text = re.sub(r'[\n]', ' ',text)
            text = re.sub(r'[[[]', ' ',text)
            text = re.sub(r'[]]]', ' ',text)
            text = re.sub(r'[-]', ' ',text)
            text = re.sub(r'[+]', ' ',text)
            text = re.sub(r'[*]', ' ',text)
            text = re.sub(r'[/]', ' ',text)
            text = re.sub(r'[//]', ' ',text)
            text = re.sub(r'[@]', ' ',text)
            text = re.sub(r'[,]', ' ',text)
            text = re.sub(r'[)]', ' ',text)
            text = re.sub(r'[-]', ' ',text)
            text = re.sub(r'[(]', ' ',text)
            text = re.sub(' + ', ' ', text)
            text = text.encode('ascii', errors='ignore').decode("utf-8")
            return text
        except Exception as e:
            print(e)
            pass

    def pdf_to_text(self,filepath):
        pdf_file = parser.from_file(filepath)

        text = pdf_file["content"]
        text = self.clean_text(text)
        return text

    def docx_to_text(self,filepath):
        text = ""
        text += docx2txt.process(filepath)
        text = self.clean_text(text)
        return text

    def txt_to_text(self,filepath):
        text = ""
        with open(filepath, mode='r', encoding='unicode_escape', errors='strict', buffering=1) as file:
            data = file.read()
        text += data
        text = self.clean_text(text)
        return text

    def prepare_training_text(self):
        training_set = []
        files = []
        for r, d, f in os.walk(self.input_data):
            for file in f:
                files.append(os.path.join(r, file))
        for doc in files:
            # self.input_data[path] = {name for name in files}
            name = doc.split('.')
            ext = name[-1]
            # print(ext)
            if ext=='docx':
                try:
                    docx_txt = self.docx_to_text(doc)
                    if docx_txt != None:
                        training_set.append(docx_txt)
                except:
                    continue

            elif ext=='pdf':
                try:
                    pdf_txt = self.pdf_to_text(doc)

                    if pdf_txt != None:
                        training_set.append(pdf_txt)
                except:
                    continue
            elif ext=='txt':
                try:
                    text = self.txt_to_text(doc)
                    if text !=None:
                        training_set.append(text)
                except:
                    continue
            else:
                continue
        # print(len(training_set))
        training_data = ' '.join(training_set)
        sent_tok_train = nltk.sent_tokenize(training_data)
        # print(sent_tok_train[0:100])
        sentences = [nltk.word_tokenize(sentence) for sentence in sent_tok_train]
        return sentences




    

training_data = W2vData()
sentences = training_data.prepare_training_text()
# print(sentences)