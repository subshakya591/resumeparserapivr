import os
import pickle
import numpy as np
import pandas as pd

from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import train_test_split

# from readText import PdfToText,DocxToText,TextExtractor
from filereader import read_cv

class SegmenterDataProcessor:

    def __init__(self):
        self.personal_info = set("personal informaion information: informations: introduction introduction:".split(' '))
        self.objective = set("summary, career summary,career objective objective: motivation motivation:".split(' '))
        self.experiences = set("work job projects summary jobs practical"
                               " responsibilities responsibilities: employment "
                               "professional career experience experience: "
                               "experiences experiences: profile"
                               " profile: profiles profiles:".split(' '))
        self.skills = set("technical skills key mainfiles professional practical skill"
                          " skill: skills skills: experience experience: experiences"
                          " experiences: competencies competencies:".split(' '))
        self.projects = set("training training: trainings trainings: college projects "
                            "projects: training trainings: attended attended:".split(' '))
        self.academics = set("education education: educational acedemic qualification"
                             " qualification: qualifications qualifications:".split(' '))
        self.rewards = set("certification certification: certifications "
                           "certifications rewards rewards:License License"
                           " honours awards Licenses Licenses:".split(' '))
        self.languages = set("language language: languages languages:".split(' '))
        self.references = set("reference reference: references references:".split(' '))

        self.links = set('links links: LINK LINK:'.split(' ')) 

        # self.trainings = set('trainings trainings: training training:'.split(' '))

        self.possible_title_keywords = self.personal_info | self.objective | self.experiences |\
                                       self.skills | self.projects | self.academics | self.rewards |\
                                       self.languages | self.references | self.links

        self.filenames_resume=[]


    # function to extract possible heading titles, classical approach by comparing the keywords in the possible headings
    # also to make a dataframe of words in the resume -> 'text' as a features for machine learning model
    def heading_features_extractor(self,text):
        print("++++++@Features@being@extracted+++++ ")
        mycolumns = ['word', 'word.istitle()', 'word.islower()',
                 'word.isupper()', 'word.endswith(":")', 'len(word)<=3',
                 'possible_title_keywords', 'title_label', 'sent_index'
                 ]

        df = pd.DataFrame(columns=mycolumns)
        try:
            sent_lines = text.split("\n")
            possible_titles = []
        # get sentence and its sentence index in resume document.
            for index, sent in enumerate(sent_lines):
            # print(index, sent)
                sent = sent.split(" ")
                sent = [x.strip() for x in sent if x.strip()]        # removes empty string in a list   
                if len(sent) < 4:
                    for word in sent:
                        df = df.append({'word': word,
                                'word.istitle()': word.istitle() | word.isupper(),
                                'word.islower()': word.islower(),
                                'word.isupper()': word.isupper(),
                                'word.endswith(":")': word.endswith(":"),
                                'len(word)<=3': len(word) <= 3,
                                'possible_title_keywords': word.lower() in self.possible_title_keywords,
                                'title_label': word.lower() in self.possible_title_keywords,
                                'sent_index': index}, ignore_index=True)

            df.replace(False, 0, inplace=True)
            df.replace(True, 1, inplace=True)
        # # returns possible titles with their sentence index in resume, and dataframe of features
        #     return possible_titles, df
        # return features dataframe only
            return df
        except Exception as e:
            print('Exception occured',e)

    def get_training_files(self):
        current_dir= os.path.dirname(os.path.abspath(__file__))
        resume_dir = (current_dir+"/Resumes/")
        for root,dirs,files in os.walk(resume_dir):
            for file in files:
                self.filenames_resume.append(resume_dir+file)
        print("<+++++@training_Files_Fetched+++++++>")

    # make a complete dataframe for training of machine learning model to extract headings
    def training_features(self):
        mycolumns = ['word', 'word.istitle()', 'word.islower()',
                 'word.isupper()', 'word.endswith(":")', 'len(word)<=3',
                 'possible_title_keywords', 'title_label', 'sent_index'
                 ]

        training_features_set = pd.DataFrame(columns=mycolumns)
 
        for file in self.filenames_resume:
            text = read_cv(file)
            

            single_resume_features = self.heading_features_extractor(text)
            training_features_set = pd.concat([training_features_set, single_resume_features], ignore_index=True)

            training_set = np.array(training_features_set[['word.istitle()', 'word.islower()', 'word.isupper()',
                                                   'word.endswith(":")', 'len(word)<=3', 'possible_title_keywords']])
            labels_set = training_features_set['title_label'].astype(int)

        return training_set, labels_set  # returns training_features_set, training_label_set


    def train(self,training_features_set, training_labels_set):
        X_train, X_test, y_train, y_test = train_test_split(training_features_set,
                                                            training_labels_set,
                                                            test_size=0.33,
                                                            random_state=42)
        gaussian = GaussianNB()
        clf = gaussian.fit(X_train, y_train)
        with open(os.getcwd() + '/models/segment_identifier.pkl', 'wb')as file:
            pickle.dump(clf, file)




a=SegmenterDataProcessor()
a.get_training_files()
training_set, labels_set=a.training_features()
a.train(training_set, labels_set)

