import os
import re
from subprocess import Popen, PIPE
from PIL import Image

from pytesseract import image_to_string
import docx2txt
from docx import Document
from tika import parser
import spacy
from nltk.tag.stanford import StanfordNERTagger
import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers.embeddings import Embedding
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report,confusion_matrix,roc_curve, precision_recall_curve, auc, make_scorer, recall_score, accuracy_score, precision_score,roc_auc_score
import matplotlib.pyplot as plt

class ResumeIdentificationTraining():

    def __init__(self):
        '''
            This method is executed once the instance of the class is created.
            Here, we have initialized the variables that are required.
        '''
        self.abspath =  os.path.dirname(os.path.abspath(__file__))
        path_1 = os.path.split(self.abspath)
        self.path_2 = os.path.split(path_1[0])
        jar = self.path_2[0] + "/common/stanfordNER/stanford-ner.jar"       
        stanford_model = self.path_2[0] + '/common/stanfordNER/final_ner_model.ser.gz'
        self.nlp = spacy.load("en_core_web_md")
        self.ner_tagger = StanfordNERTagger(stanford_model,jar, encoding='utf8')
        self.pos_files = os.listdir('Resume/')
        self.neg_files = os.listdir('NotResume/')
        self.ner_wanted = ['PER','LOC','UNI','DEG','DATE','DESIG','ORG','EXP']
        self.X_train = []
        self.Y_train = []
        self.fill_dataset()
        self.X_train = np.array(self.X_train)
        self.Y_train = np.array(self.Y_train)
        self.model = self.keras_model()
        self.roc_confusion_matrix()


    '''
        Methods to clean the document
    '''
    def clean_text(self,text):
        text = re.sub(r' +', ' ', text)
        text = re.sub(r'\t', ' \n, ', text)
        text = re.sub(r'\n+', ' \n ,', text)
        text = re.sub(r',+', ',', text)
        text = text.encode('ascii', errors='ignore').decode("utf-8")
        return text

    def pdf_to_text(self,filepath):
        pdf_file = parser.from_file(filepath)
        text = pdf_file["content"]
        text = self.clean_text(text)
        return text


    def docx_to_text(self,filepath):
        text = ""
        text += docx2txt.process(filepath)
        text = self.clean_text(text)
        return text

    def txt_to_text(self,filepath):
        text = ""
        with open(filepath, mode='r', encoding='unicode_escape', errors='strict', buffering=1) as file:
            data = file.read()
        text += data
        text = self.clean_text(text)
        return text

    def doc_to_text(self,filepath):
        text = ""
        cmd = ['antiword', filepath]
        p = Popen(cmd, stdout=PIPE)
        stdout, stderr = p.communicate()
        text += stdout.decode('utf-8', 'ignore')
        text = self.clean_text(text)
        return text

    def img_to_text(self,filepath):
        text = image_to_string(Image.open('test.png'))
        text = self.clean_text(text)
        return text

    def read_cv(self,file_path):
        image_extensions=['.jpeg','.png','.jpg','.psd','.ai']
        _,file_extension=(os.path.splitext(file_path))
        if file_extension.lower() in image_extensions:
                file_extension='.img'
        else:
            pass
        options={
                '.pdf':self.pdf_to_text,
                '.docx':self.docx_to_text,
                '.txt':self.txt_to_text,
                '.doc':self.doc_to_text
                }
        try:
            text=options[file_extension](file_path)
            return text
        except Exception as e:
            print("Exception at fileReader:"+str(e))
        
 # --------------------------------------------------------------------------------------------------------------------------------------

    
    def create_dataset(self,cleaned_text,y,ner_wanted):
        '''
            This method tokenizes the given cleaned text and tag it using the custom stanford NER model

            :param cleaned_text: Receives a cleaned text of the document
            :type cleaned_text:
            :param ner_wanted: A list of the NER tag that we want to find from the document
            :type ner_wanted: list
            :return: A numpy array representatio of the document along with its label
        '''
        x_feature = np.zeros(len(self.ner_wanted))        
        doc = self.nlp(cleaned_text)
        words = [token.text for token in doc]        
        for token, tag in self.ner_tagger.tag(words):
            for i,j in enumerate(ner_wanted):
                if j == tag:
                    x_feature[i]  = 1
                else:
                    pass        
        return x_feature,y


    def fill_dataset(self):
        '''
            This method stores the features and labels of resume and non resume documents
        '''
        for resume in self.pos_files:
            try:
                cleaned_text = self.read_cv('Resume/'+str(resume))
                x_train,y_train = self.create_dataset(cleaned_text,1,self.ner_wanted)
                self.X_train.append(np.array(x_train))
                self.Y_train.append(np.array(y_train))
            except:
                continue

        for not_resume in self.neg_files:
            try:
                cleaned_text = self.read_cv('NotResume/'+str(not_resume))
                x_train, y_train = self.create_dataset(cleaned_text,0,self.ner_wanted)
                self.X_train.append(np.array(x_train))
                self.Y_train.append(np.array(y_train))
            except:
                continue
        

    
    def keras_model(self):
        '''
            This method takes the dataset and splits it into the training and testing dataset.
            Then using training dataset the model is created and validated using the testing dataset.
            For creating the model LSTM has been used. After the completion of the model, it has been saved.

            :return: This method returns a keras model
        '''
        train_x,self.test_x,train_y,self.test_y = train_test_split(self.X_train,self.Y_train,test_size = 0.2,random_state = 100)
        train_y = np.array(train_y)
        train_y = train_y.reshape(len(train_y),1)        
        no_of_resume = self.X_train.shape[0]
        embedding_vector_length = 32
        classifier = Sequential()
        classifier.add(Embedding(no_of_resume,embedding_vector_length))
        classifier.add(LSTM(50))
        classifier.add(Dense(1,activation='sigmoid',kernel_initializer='random_normal'))
        classifier.compile(optimizer='adam',loss='binary_crossentropy',metrics=['accuracy'])
        classifier.fit(train_x,train_y,validation_data=(self.test_x,self.test_y),batch_size=128,epochs=100)
        classifier.save(self.path_2[0] + "/cvparser/models/Resume_Identification_Model.h5")
        print ("Model Training Completed and Saved")
        return classifier

    
    def roc_confusion_matrix(self):
        '''
            This method is created to check the accuracy of the model.
            This method will show out the following things: -
            - A table showing the precision, recall and f1-score
            - A ROC cruve
            - A list of confusion matrix a the given threshold
        '''
        test = self.model.predict(self.test_x)
        print ("Using the 0.5 as the threshold")
        output_value = test > 0.5
        print (classification_report(self.test_y,output_value))
        fpr,tpr,threshold_value = roc_curve(self.test_y,test)
        plt.figure(figsize=(8,4))
        plt.xlabel('FPR')
        plt.ylabel('TPR')
        plt.plot(fpr, tpr)
        for i in threshold_value:
            test_1 = test > i
            print ("Threshold: ",i)
            print (confusion_matrix(self.test_y,test_1))


create_model = ResumeIdentificationTraining()