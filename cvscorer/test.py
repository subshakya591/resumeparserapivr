from resumeparsernew.cvscorer.calculatescore import ScoreGenerator
import datetime

a = ScoreGenerator()

data={
        'personal_data':
            {
                'id': 1, 'user': 1, 'first_name': 'Pujan', 'middle_name': None, 'last_name': 'Maharjan', 'dob': datetime.date(1995, 3, 9), 'nationality': 'Nepali', 'gender': 1, 'address': 'Kathmandu, Nepal', 'zip_code': '44700', 'contact': '9843815209', 'carrer_objective': None, 'resume': None, 'linkedin': None, 'github': None
            },
        'work':
            [
                {
                'id': 28, 'user_id': 1,
                 'job_title': 'React Developer',
                 'organization': 'NewTech Solution',
                 'location': 'Newyork, Manhattan',
                 'responsibilities': 'Develop app using react framework',
                 'from_date': datetime.date(2004, 2, 17),
                 'to_date': datetime.date(2005, 3, 18),
                 'currently_working': False
                },
                {'id': 29,
                'user_id': 1,
                'job_title': 'AI developer',
                'organization': 'Infodev',
                 'location': 'Kathmandu',
                 'responsibilities': 'To develop AI modules',
                  'from_date': datetime.date(2008, 3, 20),
                  'to_date': datetime.date(2013, 5, 29),
                 'currently_working': False},
                {'id': 6,
                 'user_id': 1,
                 'job_title': 'Django Programmer',
                  'organization': 'Infodev Pvt. Ltd.',
                  'location': 'Kathmandu', 'responsibilities': 'heloo', 'from_date': datetime.date(2016, 1, 14),
                  'to_date': datetime.date(2019, 3, 21), 'currently_working': False},
                {'id': 1, 'user_id': 1, 'job_title': 'Python Developer', 'organization': 'Colorado IT solutions', 'location': 'Kathmandu', 'responsibilities': 'To develop django programs', 'from_date': datetime.date(2019, 1, 1), 'to_date': None, 'currently_working': True}
            ],
        'skills':
            [
                'pandas', 'html', 'css', 'tensorflow', 'pytorch', 'javascript', 'jquery', 'python', 'numpy'
            ],
        'education':
            [
                {'id': 2, 'user_id': 1, 'program': 'Computer Engineering', 'institution': 'Tribhuvan Univerisity', 'location': 'Kathmandu', 'degree': 'Bachelors Degree', 'from_date': datetime.date(2013, 1, 3), 'to_date': datetime.date(2017, 6, 14)},
                {'id': 1, 'user_id': 1,
                'program': 'Computer Engineering',
                 'institution': 'Harvard University',
                 'location': 'Harvard',
                 'degree': 'Masters',
                 'from_date': datetime.date(2018, 3, 14),
                  'to_date': None}
            ],
        'trainings':
            [
                {'id': 2, 'user_id': 1, 'name': 'Advanced Python', 'institution': 'Harvard IT training Institute', 'start_date': datetime.date(2018, 9, 11), 'end_date': datetime.date(2019, 1, 1)},
                {'id': 4, 'user_id': 1, 'name': 'Advance Machine Learning', 'institution': 'MIT Training Centre', 'start_date': datetime.date(2019, 1, 18), 'end_date': datetime.date(2019, 2, 24)},
                {'id': 7, 'user_id': 1, 'name': 'Advanced Django', 'institution': 'Harvard IT Institute', 'start_date': datetime.date(2012, 2, 15), 'end_date': datetime.date(2014, 3, 19)}
            ],
        'tech_exp':
            [
                '1+ years of Java Programming', '2 years of Python Programming', '3 years of swift programming', '1 year of C++ Experience', '1+ years of Golang Experience'
            ],
        'designations': ['Python Developer', 'Designer'],
        'user_skills': ['python', 'Django', 'HTML', 'CSS', 'javascript', 'Numpy']
    }

data2=[

    {

        'description': 'Code, script and au...t issues.',

        'job_designation': 'Python Developer',

        'job_pk': '3',

        'primary_skills': ['python'],

        'requirements': 'Must have strong wr...rk ethic.',

        'responsibilities': 'Assist with unit te...rocesses.',

        'secondary_skills': [],

        'skills_qualification': 'Strong script devel... required'

    },

    {

        'description': '',

        'job_designation': 'Python Developer',

        'job_pk': '53',

        'primary_skills': ['Pandas'],

        'requirements': 'Et rerum non consequ',

        'responsibilities': 'Est quia laborum S',

        'secondary_skills': [],

        'skills_qualification': 'Temporibus a at do v'

    },

    {

        'description': '',

        'job_designation': 'Python Developer',

        'job_pk': '42',

        'primary_skills': ['numpy'],

        'requirements': 'Eveniet sed et elit',

        'responsibilities': 'Illum atque in unde',

        'secondary_skills': ['communication'],

        'skills_qualification': 'Omnis dolorem labori'

    }

]
score = a.getScore(data2,data)
print(score)


