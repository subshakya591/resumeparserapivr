import datetime
import nltk
from nltk.tag import pos_tag
from nltk.tokenize import word_tokenize
from nltk import RegexpParser

def getxp(worklist):
    '''
    get the list containing the dictionary of the
    work experience of user. Extracts all the working
    experience number of years and then returns the total
    experience
    :param worklist:
    :return: total years of experience :type int
    '''
    finalworkxp = []
    for work in worklist:
        if work.get('currently_working') == True:
            to_date = datetime.date.today()

            exp = to_date - work.get('from_date')
        else:

            exp = work.get('to_date') - work.get('from_date')
        years_of_xp = str(round((exp.days/365),2)) + work.get('job_title')
        finalworkxp.append(years_of_xp)
    return finalworkxp

def experiencefinder(description_section):
    '''
    function that takes the job description section of the
    posted job and uses the nltk regex chunker to chunk the
    experience out from the section
    :param description_section:type str
    :return:provides the list of experience chunked
    '''
    tokens = word_tokenize(description_section)
    tagged_tokens = pos_tag(tokens)
    chunk_rule_a = "experience_type_a:{<CD><N.+><IN><J.+|V.+>?<N.+><IN>?<N.+>+}"
    chunk_rule_b = "experience_type_b:{<N.+>+<\(>*<CD><N.+><N.+>?<IN>?<J.+|VBG>?<N.+>?<\)>*}"
    chunk_rule_c = "experience_type_c:{<N.+>+<\:>*<CD><N.+><N.+>?<IN>?<J.+|VBG>?<N.+>?<N.+>*?}"
    cpa = RegexpParser(chunk_rule_a)
    cpb = RegexpParser(chunk_rule_b)
    cpc = RegexpParser(chunk_rule_c)
    chunked_phrases = []
    if tagged_tokens:
        exp_a = cpa.parse(tagged_tokens)
        exp_b = cpb.parse(tagged_tokens)
        exp_c = cpc.parse(tagged_tokens)
        for subtree in exp_a:
            if type(subtree) == nltk.Tree and subtree.label() == "experience_type_a":
                chunked_phrases.append(" ".join([token for token,pos in subtree.leaves()]))
        for subtree in exp_b:
            if type(subtree) == nltk.Tree and subtree.label() == "experience_type_b":
                chunked_phrases.append(" ".join([token for token, pos in subtree.leaves()]))
        for subtree in exp_c:
            if type(subtree) == nltk.Tree and subtree.label() == "experience_type_c":
                chunked_phrases.append(" ".join([token for token, pos in subtree.leaves()]))
        return (chunked_phrases)



def formatProfile(userprofile):
    '''
    All the data from the profile will be
    accumulated here and the formatted profile
    will be obtained
    :param userprofile
    :type userprofile:dict
    :return : This method returns the formatted
            user profile in dictionary format
    '''

    has_designation = [work.get('job_title') for work in
                       userprofile.get('work')]
    has_responsibilities = [work.get('responsibilities') for work in
                            userprofile.get('work')]
    has_skills = userprofile.get('skills')
    has_skills.extend(userprofile.get('user_skills'))
    has_degree = [education.get('degree') for education
                  in userprofile.get('education')]

    has_experience = userprofile.get('tech_exp')
    total_experience = getxp(userprofile.get('work'))
    return {'designation':has_designation,
            'role':has_responsibilities,
            'skills':has_skills,
            'degree':has_degree,
            'tech_exp':has_experience,
            'experience':total_experience}

def formatJob(job_details):
    '''
    It receives the job description or details
    and formats it into the proper format for scoring
    :param job_details
    :type job_details: dict
    :returns : This method returns job's primary key as str, designation required for job as str,
    required primary skills and secondary skills as list and roles and responsibilities required as
    list
    '''
    req_job_pk = job_details.get('job_pk')
    req_designation = job_details.get('job designation')
    req_primary_skills = job_details.get('primary_skills')
    req_secondary_skills = job_details.get('secondary_skills')
    responsibilities_description_requirements = job_details.get('requirements')+\
                      job_details.get('responsibilities')+\
                      job_details.get('description')

    req_experience = experiencefinder(responsibilities_description_requirements)

    #req_xp = yet to come.
    return (req_job_pk,req_designation,req_primary_skills,\
           req_secondary_skills, responsibilities_description_requirements,req_experience)


def normalizeScore(kwargs):
    '''
    This code here takes the score for different
    section of the user profile and then assign
    some standard weights and then normalize them
    to 100
    :param : kwargs
    :type kwargs : dict
    :return This function then returns normalized total score whose
            data type is float
     '''

    norm_primary_skill_score = kwargs.get('primeskillscore') * 0.8
    norm_secondary_skill_score = kwargs.get('secskillscore') * 0.2
    norm_total_skill_score = norm_primary_skill_score + norm_secondary_skill_score
    norm_exp_score = kwargs.get('xpscore')
    norm_academic_score = kwargs.get('academic_score')
    norm_role_score = kwargs.get('rolescore')
    norm_total_score = int((norm_total_skill_score * 0.6) + (norm_exp_score * 0.2)\
                            + (norm_academic_score * 0.15) + (norm_role_score * 0.05 ))
    if norm_total_score < 0:
        return 10
    elif norm_total_score >100:
        return 98
    else:
        return norm_total_score


