from nltk.tag.stanford import StanfordNERTagger
from gensim.models import Word2Vec
from resumeparsernew.cvscorer.dataprepare import formatJob,formatProfile,normalizeScore
from resumeparsernew.cvscorer.scoregenerator import calculateSkillScore,calculateVecScore, calculateAcademicScore
from resumeparsernew import configuration as cfg


class ScoreGenerator:

    def __init__(self):
        self.word2vec = Word2Vec.load(cfg.Word2vecModelPath)
        jar = cfg.jarPath
        model_ner = cfg.NerModelPath
        self.ner_tagger = StanfordNERTagger(model_ner, jar, encoding='utf8')


    def getScore(self,jobDescriptions,profile):
        preparedJobs = map(formatJob,jobDescriptions)
        preparedProfile = formatProfile(profile)
        allProfileScore = []

        for (pk,desig,pskills,secskills,responsibilities_descriptions_requirements,required_exp) in preparedJobs:

            ''' 
            contains job_pk,job_primary_skill,job_designation,
            job_primary_skills,job_secondary_skills,
            responsibilites and job descriptions
            '''

            prime_skill_score,sec_skill_core,matched_skills = \
                calculateSkillScore(preparedProfile.get('skills'),
                                   pskills,secskills)
            role_score = calculateVecScore(self.word2vec,preparedProfile.get('role'),#list of strings
                                  responsibilities_descriptions_requirements)#string
            # xp_score = calculateXpScore(responsibilities_descriptions_requirements,preparedProfile.get('tech_exp'))
            xp_score = 0
            academic_score = calculateAcademicScore(self.word2vec,self.ner_tagger,
                                                    responsibilities_descriptions_requirements,
                                                    preparedProfile.get('degree'))
            unformatted_score = {
                                  'primeskillscore' : prime_skill_score,
                                  'secskillscore' : sec_skill_core,
                                  'xpscore' : xp_score,
                                  'academic_score' : academic_score,
                                  'rolescore':role_score
                                   }
            normalized_score = normalizeScore(unformatted_score)


            allProfileScore.append({'pk':pk,'score':normalized_score,'skillsmatched':matched_skills})
        return allProfileScore




