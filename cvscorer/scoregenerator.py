import numpy as np
import math
from scipy import spatial

from resumeparsernew.cvscorer.dataprepare import experiencefinder

def calculateSkillScore(profileskills,jobprimaryskills,
                       jobsecondarySkills):

    '''
    This functions here generates the score for the skills that the job seeker possesses
    :param profileskills : Skill of job seeker, jobprimarySkills:primary skills from job description,
                           jobsecondarySkills:job description secondary
    :type profileskills:str, jobprimaryskills:str, jobsecondarySkills:str
    :returns: primarySkillScore : score from primary skills, secondarySkillScore : score from secondary Skills,
            matchedSkills : Required Skills Present in profile
    '''
    normalized_primary_skill = [ skill.lower() for skill in jobprimaryskills]
    normalized_secondary_skill = [skill.lower() for skill in jobsecondarySkills]
    availablePrimaryScore = [skill for skill in profileskills if skill.lower()
                             in normalized_primary_skill]
    availableSecondaryScore = [skill for skill in profileskills if skill.lower()
                               in normalized_secondary_skill]
    total_skills = normalized_primary_skill + normalized_secondary_skill
    primarySkillScore = 0
    secondarySkillScore = 0
    if jobprimaryskills:
        primarySkillScore = (len(availablePrimaryScore)/len(jobprimaryskills))*100

    if jobsecondarySkills:
        secondarySkillScore = (len(availableSecondaryScore)/len(jobsecondarySkills))*100

    matchedSkills = {skill:(True if skill.lower() in total_skills else False) for skill in profileskills}
    return primarySkillScore,secondarySkillScore,matchedSkills

def calculateVecScore(model,roles,required_responsibilities):
    job_word2vec =[]
    job_tokens = required_responsibilities.split()
    role_word2vec = []
    for token in job_tokens:
        try:
            job_word2vec.append(model.wv[token.lower()])
        except Exception as e:
            pass
    job_word2vec = np.mean(job_word2vec,axis = 0)
    if roles:
        for role in roles:
            role_w2v=[]
            role_tokens = role.split()
            for token in role_tokens:
                try:
                    word_small = token.lower()
                    role_w2v.append(model.wv[word_small])
                except Exception as e:
                    print(e)
                    pass
            role_word2vec.append((np.mean(role_w2v,axis= 0),'score'))
    retrieval = set()
    for i in range(len(role_word2vec)):
        retrieval.add(1 - spatial.distance.cosine(job_word2vec,role_word2vec[i][0]))
    score = max(retrieval)
    if score < 0 or math.isnan(score):
        return 0
    else:
        return score


def calculateAcademicScore(word2vecmodel,ner_tagger,job_description,degrees):
    words = job_description.split()
    tagged_tuples = ner_tagger.tag(words)
    alldegree = []
    deg_container = []
    for word, tag in tagged_tuples:
        if tag == 'DEG':
            deg_container.append(word)
        else:
            if deg_container:
                degree = ' '.join(deg_container)
                alldegree.append(degree)
                break

    user_profile_word2vec = []
    if alldegree:
        degree_w2v = []
        degree_tokens =  alldegree[0].split()
        for token in degree_tokens:
            try:
                degree_w2v.append(word2vecmodel.wv[token])
            except:
                try:
                    word_small = token.lower()
                    degree_w2v.append(word2vecmodel.wv[word_small])
                except Exception as e:
                    print(e)
                    pass

        job_academic_word2vec = np.mean(degree_w2v,axis= 0)

        for profile_deg in degrees:
            degree_w2v = []
            degree_tokens = profile_deg.split()
            for token in degree_tokens:
                try:
                    word_small = token.lower()
                    degree_w2v.append(word2vecmodel.wv[word_small])
                except Exception as e:
                    print(e)
                    pass

            user_profile_word2vec.append(((np.mean(degree_w2v, axis=0)), 'score'))
        retrieval = set()
        for i in range(len(user_profile_word2vec)):
            retrieval.add((1 - spatial.distance.cosine(job_academic_word2vec, user_profile_word2vec[i][0]), user_profile_word2vec[i][1]))
        score = max(retrieval)
        if score[0] < 0 or math.isnan((score[0])):
                score = 0
        else:
            score = math.floor(score[0] * 100)

        return score
    else:
        return 100


def calculateXpScore(job_desription,user_xp):
    req_xp = experiencefinder(job_desription)
    user_xp = user_xp
    return 10








