{
    "PERSONAL_INFORMATION": {
        "First_Name": "Anonymous",
        "Last_Name": "Anonymous",
        "Address": [
            "Nepali , Maithili , Bhojpuri",
            "Parsa , Birgunj , Nepal"
        ],
        "Email": [
            "karn.ashish04@gmail.com"
        ],
        "Phone_Number": [
            "+977-9815200670",
            "2049-11-21"
        ],
        "Zip_Code": [],
        "Nationality": [
            "Nepalese"
        ],
        "github": [],
        "linkedin": [],
        "birthdate": [
            "2049-11-21"
        ],
        "Gender": [
            "Male"
        ]
    },
    "OBJECTIVE": "To enhance professional skills, business efficiency to develop my potentiality and lead the exponential growth of organizational development in the best possible way with sheer determination and commitment.  ",
    "SKILLS": [
        "Java",
        "Photoshop",
        "Sublime",
        "C",
        "Php",
        "Html",
        "Mysql"
    ],
    "EDUCATION": {},
    "EXPERIENCE": {
        "Exp1": {
            "Designation": "Account Supervisor",
            "entry_date": "unknown",
            "exit_date": "08-11-2012",
            "roles": "EXPERIENCE at Himal Stationery & Paper Products Pvt . Ltd . Since July ,"
        }
    },
    "LANGUAGES": [
        "Bhojpuri",
        "English",
        "Hindi",
        "Maithili",
        "Nepali"
    ],
    "PROJECTS": "  1 day workshop on Leadership & Effective Communication by Prabha Raman Foundation on Sep 09, 2014.  1 day workshop on Motivational program organized by Young Mens Buddhist Association, Nepal.   ",
    "REWARDS": "",
    "REFERENCES": "OmkarNath Gupta  Principal, ACME College of Engineering  Email: info@acmecollege.edu.np  Mob: 9860302426  Tel: 01-4381335  Declaration:-  I hereby declare that the information furnished above is true to the best of my knowledge and belief.  AshishK.  AshishKarn"
}