import nltk
from cvparser.formatter.formatdata import formatEducationalinfo,formatExperienceinfo,cleandata



class StanfordNER:

    ###----<NER model that chooses the particular model and parser>----###
    @staticmethod
    def ner_parser(model,text,mode):
        words = nltk.word_tokenize(text)
        parser = {
                'profile':StanfordNER.personal_info_parser,
                'academics':StanfordNER.education_parser,
                'experience':StanfordNER.experience_parser,
                }

        tagged_tuples = model.tag(words)
        return parser[mode](tagged_tuples,text)

    @staticmethod
    def experience_parser(tagged_tuples,text):
        o_counter = 0
        alldesignation = []
        alllocations = []
        alldate = []
        allroles =[]
        allcompany= []
        desig_container = []
        loc_container = []
        date_container = []
        roles_container = []
        company_container = []
        for word,tag in tagged_tuples:
            if tag == 'DESIG':
                desig_container.append(word)
            else:
                if desig_container:
                    designation = ' '.join(desig_container)
                    alldesignation.append(designation)
                    desig_container = []
            if tag == 'LOC':
                loc_container.append(word)

            else:
                if loc_container:
                    location = ' '.join(loc_container)
                    loc_container = []
                    alllocations.append(location)
            if tag == 'DATE':
                date_container.append(word)

            else:
                if date_container:
                    date = ' '.join(date_container)
                    alldate.append(date)
                    date_container = []

            if tag == 'O':
                o_counter += 1
                roles_container.append(word)
            else:
                if o_counter > 10:
                    role = ' '.join(roles_container)
                    roles_container = []
                    o_counter = 0
                    allroles.append(role)

            if tag == 'ORG':
                company_container.append(word)
            else:
                if company_container:
                    company = ' '.join(company_container)
                    company_container = []
                    allcompany.append(company)
        if loc_container:
            alllocations.append(' '.join(loc_container))

        if date_container:
            alldate.append(' '.join(date_container))

        if company_container:
            allcompany.append(' '.join(company_container))

        if desig_container:
            alldesignation.append(' '.join(desig_container))
        if roles_container:
            allroles.append(' '.join(roles_container))
        sent_tokens = text.split('\n')
        sent2indx = {}
        item_tracker = {}
        for index,sentence in enumerate(sent_tokens):
            cleaned_sentence = cleandata(sentence)
            if cleaned_sentence not in sent2indx:
                sent2indx.update({cleaned_sentence:index})
                item_tracker.update({cleaned_sentence:1})

            else:

                sent2indx.update({cleaned_sentence+'{}'.format(item_tracker[cleaned_sentence]):index})
                item_tracker[cleaned_sentence] += 1



        experiences = formatExperienceinfo(sent_tokens,sent2indx,alldesignation,
                                          allcompany,alldate,alllocations,allroles)
        return experiences



    @staticmethod
    def education_parser(tagged_tuples,text):
        alldegree = []
        alllocations = []
        alldate = []
        alluniversity = []
        deg_container = []
        loc_container = []
        date_container = []
        university_container = []
        for word,tag in tagged_tuples:
            if tag == 'DEG':
                deg_container.append(word)
            else:
                if deg_container:
                    degree = ' '.join(deg_container)
                    alldegree.append(degree)
                    deg_container = []
            if tag == 'LOC':
                loc_container.append(word)
            else:
                if loc_container:
                    location = ' '.join(loc_container)
                    loc_container = []
                    alllocations.append(location)
            if tag == 'DATE':
                date_container.append(word)
            else:
                if date_container:
                    date = ' '.join(date_container)
                    alldate.append(date)
                    date_container = []
            if tag == 'UNI':
                university_container.append(word)
            else:
                if university_container:
                    university = ' '.join(university_container)
                    university_container = []
                    alluniversity.append(university)

        if loc_container:
            alllocations.append(' '.join(loc_container))

        if date_container:
            alldate.append(' '.join(date_container))

        if university_container:
            alluniversity.append(' '.join(university_container))

        if deg_container:
            alldegree.append(' '.join(deg_container))

        sent_tokens = text.split('\n')
        sent2indx = {}
        item_tracker = {}
        for index, sentence in enumerate(sent_tokens):
            cleaned_sentence = cleandata(sentence)
            if cleaned_sentence not in sent2indx:
                sent2indx.update({cleaned_sentence: index})
                item_tracker.update({cleaned_sentence: 1})

            else:

                sent2indx.update({cleaned_sentence + '{}'.format(item_tracker[cleaned_sentence]): index})
                item_tracker[cleaned_sentence] += 1
        academics = formatEducationalinfo(
                                          sent_tokens, sent2indx, alldegree,
                                          alluniversity, alldate, alllocations
                                          )


        return academics




    @staticmethod
    def personal_info_parser(tagged_tuples,text):
        name = 'Anonymous'
        address = []
        possible_name = []
        possible_address = []

        for word, tag in tagged_tuples:
            if tag == "PER":
                possible_name.append(word)
            else:
                if possible_name:
                    name = (" ".join(possible_name)).lower()
                    possible_name = []

            if tag == "LOC":
                possible_address.append(word)
            else:
                if possible_address:
                    address.append(" ".join(possible_address))
                    possible_address = []
        final_address = list(set(address))
        formatted_name = name.title()
        return formatted_name, final_address

    # @staticmethod
    # def education_parser(tagged_tuples):
    #     possible_university=[]
    #     possible_degree=[]
    #     possible_date=[]
    #     possible_location=[]
    #     university_name={}
    #     university_degree={}
    #     university_loc={}
    #     date={}
    #     deg_idx=0
    #     date_idx=0
    #     loc_idx=0
    #     uni_idx=0
    #     academics={}
    #     for word,tag in tagged_tuples:
    #         if tag=="DEG":
    #             possible_degree.append(word)
    #
    #         else:
    #             if possible_degree:
    #                 university_degree.update({" ".join(possible_degree):deg_idx})
    #                 deg_idx+=1
    #
    #                 possible_degree=[]
    #
    #     if possible_degree:
    #         university_degree.update({" ".join(possible_degree): deg_idx})
    #         deg_idx += 1
    #
    #
    #     for word,tag in tagged_tuples:
    #         if tag=="DATE":
    #             possible_date.append(word)
    #         else:
    #             if possible_date:
    #                 date.update({" ".join(possible_date):date_idx})
    #                 date_idx+=1
    #                 possible_date = []
    #     if possible_date:
    #         date.update({" ".join(possible_date):date_idx})
    #         date_idx+=1
    #
    #     for word,tag in tagged_tuples:
    #         if tag=="UNI":
    #             possible_university.append(word)
    #         else:
    #             if possible_university:
    #                 university_name.update({" ".join(possible_university):uni_idx})
    #                 uni_idx+=1
    #                 possible_university=[]
    #     if possible_university:
    #         university_name.update({" ".join(possible_university):uni_idx})
    #
    #     for word,tag in tagged_tuples:
    #         if tag=="LOC":
    #             possible_location.append(word)
    #         else:
    #             if possible_location:
    #                 university_loc.update({" ".join(possible_location):loc_idx})
    #                 loc_idx+=1
    #                 possible_location=[]
    #         if possible_location:
    #             university_loc.update({" ".join(possible_location):loc_idx})
    #
    #
    #     x=1
    #     for key,value in university_degree.items():
    #         academics.update({"E{}".format(x):{"Degree":key}})
    #         for ky,val in university_name.items():
    #             if value==val:
    #                 academics["E{}".format(x)].update({"university":ky})
    #         for lky,lval in university_loc.items():
    #             if value==lval:
    #                 academics["E{}".format(x)].update({"location":lky})
    #         for dky,dval in date.items():
    #             # dky=format_date(dky)
    #             if len(university_degree)==len(date) and value==dval:
    #                 academics["E{}".format(x)].update({"graduated_year":dky})
    #                 break
    #             else:
    #                 enrolled_date={}
    #                 exit_date={}
    #                 i=0
    #                 en_count=0
    #                 ex_count=0
    #                 for k,v in date.items():
    #                     if i%2==0:
    #                         enrolled_date.update({k:en_count})
    #                         i+=1
    #                         en_count+=1
    #                     else:
    #                         exit_date.update({k:ex_count})
    #                         ex_count+=1
    #                         i+=1
    #                         for ek,ev in enrolled_date.items():
    #                             # ek=format_date(ek)
    #                             if value==ev:
    #                                 academics["E{}".format(x)].update({"enrolled_date":ek})
    #                         for gk,gv in exit_date.items():
    #                             # gk=format_date(gk)
    #                             if value==gv:
    #                                 academics["E{}".format(x)].update({"graduated_date":gk})
    #
    #         x+=1
    #     return(academics)
    #
    # @staticmethod
    # def experience_parser(tagged_tuples):
    #     possible_location=[]
    #     possible_company = []
    #     possible_designation = []
    #     possible_date = []
    #     locations={}
    #     ex_company_name = {}
    #     designation = {}
    #     date = {}
    #     deg_idx = 0
    #     date_idx = 0
    #     com_idx = 0
    #     loc_idx=0
    #     experience = {}
    #
    #     for word, tag in tagged_tuples:
    #         if tag == "DESIG":
    #             possible_designation.append(word)
    #         else:
    #             if possible_designation:
    #                 designation.update({" ".join(possible_designation): deg_idx})
    #                 deg_idx += 1
    #                 possible_designation = []
    #
    #     for word, tag in tagged_tuples:
    #         if tag == "DATE":
    #             possible_date.append(word)
    #         else:
    #             if possible_date:
    #                 date.update({" ".join(possible_date): date_idx})
    #                 date_idx += 1
    #                 possible_date = []
    #     if possible_date:
    #         date.update({" ".join(possible_date): date_idx})
    #         date_idx += 1
    #
    #     for word, tag in tagged_tuples:
    #         if tag == "ORG":
    #             possible_company.append(word)
    #         else:
    #             if possible_company:
    #                 ex_company_name.update({" ".join(possible_company): com_idx})
    #                 com_idx += 1
    #                 possible_company = []
    #     if possible_company:
    #         ex_company_name.update({" ".join(possible_company): date_idx})
    #
    #     for word, tag in tagged_tuples:
    #         if tag == "LOC":
    #             possible_location.append(word)
    #         else:
    #             if possible_location:
    #                 locations.update({" ".join(possible_location): loc_idx})
    #                 loc_idx += 1
    #                 possible_location = []
    #     if possible_location:
    #         locations.update({" ".join(possible_location): loc_idx})
    #
    #     x = 1
    #     for key, value in designation.items():
    #         experience.update({"Exp{}".format(x): {"Designation": key}})
    #         for ky, val in ex_company_name.items():
    #             if value == val:
    #                 experience["Exp{}".format(x)].update({"company": ky})
    #         for lky,lval in locations.items():
    #             if value==lval:
    #                 experience["Exp{}".format(x)].update({"location":lky})
    #         for dky, dval in date.items():
    #             # dky=format_date(dky)
    #             if len(designation) == len(date) and value == dval:
    #                 experience["Exp{}".format(x)].update({"exit year": dky})
    #                 break
    #             else:
    #                 entry_date = {}
    #                 exit_date = {}
    #                 i = 0
    #                 en_count = 0
    #                 ex_count = 0
    #                 for k, v in date.items():
    #                     if i % 2 == 0:
    #                         entry_date.update({k: en_count})
    #                         i += 1
    #                         en_count += 1
    #                     else:
    #                         exit_date.update({k: ex_count})
    #                         ex_count += 1
    #                         i += 1
    #                         for ek,ev in entry_date.items():
    #                             # ek=format_date(ek)
    #                             if value == ev:
    #                                 experience["Exp{}".format(x)].update({"entry_date": ek})
    #                         for gk, gv in exit_date.items():
    #                             # gk=format_date(gk)
    #                             if value == gv:
    #                                 experience["Exp{}".format(x)].update({"exit_date": gk})
    #
    #         x += 1
    #     # import pdb;pdb.set_trace()
    #
    #     return (experience)

    #
    #
    #
    #


